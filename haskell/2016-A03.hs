module Main where

import Data.List.Split
import Data.List

type Triangle = (Int, Int, Int)

valid :: Triangle -> Bool
valid (a, b, c) = let sorted   = sort [a, b, c]
                      largest  = last sorted
                      smallest = sum . take 2 $ sorted
                  in largest < smallest

p1Parser :: String -> [Triangle]
p1parser s =
  where
  let (x:y:z:[]) = read <$> filter (/= "") $ splitOn " " s $ words s
             in (x, y, z)



validTris :: [Triangle] -> Int
validTries = filter valid
main :: IO ()
main = do
  input <- getContents
  putStrLn $ validTries $ p1Parser input
  putStrLn $ validTries $ p2Parser input
                      
