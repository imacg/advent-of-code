{-# LANGUAGE OverloadedStrings #-}
module Main where

import Crypto.Hash
import Crypto.Hash.Algorithms
import qualified Data.Map as M
import qualified Data.ByteString.Char8 as B8

prefixHash :: String -> Int -> String
prefixHash prefix ix = show $ (hash (B8.pack (prefix ++ show ix)) :: Digest MD5)

candidates prefix = map (drop 5) . filter (\x -> take 5 x == "00000") . map (prefixHash prefix) $ [0..]
p1solution = map head . take 8 . candidates

extract :: String -> Maybe (Int, Char)
extract (p:v:_) | '0' <= p && p < '8' = Just (read [p], v)
                | otherwise           = Nothing

p2solution :: String -> M.Map Int Char              
p2solution input = p2sol' M.empty (candidates input)
  where
    p2sol' :: M.Map Int Char -> [String] -> M.Map Int Char
    p2sol' m (x:xs) | M.size m == 8 = m
                    | otherwise     = case extract x of Just (p, v) -> if M.notMember p m then p2sol' (M.insert p v m) xs else p2sol' m xs
                                                        Nothing     -> p2sol' m xs
main :: IO ()
main = do
  let input = "ojvtpuvg"
  print $ p1solution input
  print $ p2solution input
