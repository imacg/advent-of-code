module Main where

import Data.List.Split (splitOn)

type Present = (Int, Int, Int)

paper :: Present -> Int
paper (a, b, c) =  let sides = [a*b, a*c, b*c]
                       area  = 2 * sum sides
                       face  = minimum sides
                   in area + face

ribbon :: Present -> Int
ribbon (a, b, c) = let sides  = [a + b, a + c, b + c]
                       volume = a * b * c
                   in 2 * minimum sides + volume
                      
present :: String -> Present
present l = case map read (splitOn "x" l) of [a, b, c] -> (a, b, c)
                                             _         -> error "bad input (or parser :p)"

main :: IO ()
main = do
  presents <- fmap present <$> lines <$> getContents
  putStrLn $ "P1 Solution: " ++ show (sum (map paper presents))
  putStrLn $ "P2 Solution: " ++ show (sum (map ribbon presents))
