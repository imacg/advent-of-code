module Main where

import Data.List (transpose, sortOn, group, sort)

mostCommon :: String -> Char
mostCommon = head . head . sortOn (\x -> - length x) . group . sort

leastCommon :: String -> Char
leastCommon = head . head . sortOn length . group . sort

main :: IO ()
main = do
  input <- lines <$> getContents
  let columns = transpose input
  putStrLn $ map mostCommon columns
  putStrLn $ map leastCommon columns
