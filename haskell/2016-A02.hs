module Main where

import Data.Maybe
import qualified Data.Map as M
import Control.Monad.RWS

type Position = (Int, Int) -- x,y
type Keypad = M.Map Position Char

-- MONADS - cause why not?
type Puzzle = RWS Keypad String Position

square :: Keypad
square  = M.fromList [ ((0, 0), '1')
                     , ((1, 0), '2')
                     , ((2, 0), '3')
                     , ((0, 1), '4')
                     , ((1, 1), '5')
                     , ((2, 1), '6')
                     , ((0, 2), '7')
                     , ((1, 2), '8')
                     , ((2, 2), '9')]

diamond :: Keypad
diamond = M.fromList [((2, 0), '1')
                     ,((1, 1), '2')
                     ,((2, 1), '3')
                     ,((3, 1), '4')
                     ,((0, 2), '5')
                     ,((1, 2), '6')
                     ,((2, 2), '7')
                     ,((3, 2), '8')
                     ,((4, 2), '9')
                     ,((1, 3), 'A')
                     ,((2, 3), 'B')
                     ,((3, 3), 'C')
                     ,((2, 4), 'D')]
          
parse :: Char -> Puzzle ()
parse 'U' = move ( 0, -1)
parse 'D' = move ( 0,  1) 
parse 'L' = move (-1,  0)
parse 'R' = move ( 1,  0)

valid :: Keypad -> Position -> Bool
valid kp pos = pos `M.member` kp

move :: Position -> Puzzle ()
move delta = do
  pos <- get
  kp <- ask
  let pos' = (fst pos + fst delta, snd pos + snd delta)
  if valid kp pos' then put pos'
  else return ()

press :: Puzzle ()
press = do
  pos <- get
  kp <- ask
  let digit = fromJust $ M.lookup pos kp
  tell [digit]

solve :: Keypad -> Position -> Puzzle a -> String
solve kp pos puz = snd $ evalRWS puz kp pos

main :: IO ()
main = do
  inputs <- lines <$> getContents
  let f = mapM_ (\xs -> mapM_ parse xs >> press) inputs
  putStrLn $ solve square (1, 1) f
  putStrLn $ solve diamond (0, 2) f
