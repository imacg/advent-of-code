module Main where

import Data.Maybe (fromJust)
import Data.List (findIndex)

santaFloors :: String -> [Int]
santaFloors = scanl inst 0
  where
    inst f '(' = f + 1
    inst f ')' = f - 1
    inst _ _   = error "bad input"

main :: IO ()
main = do
  input <- getContents
  let trail = santaFloors input
  putStrLn $ "P1 solution: " ++ show (last trail)
  putStrLn $ "P2 solution: " ++ show (fromJust $ findIndex (== -1) trail)
    
