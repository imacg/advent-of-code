module Main where

import Data.List.Split (splitOn)
import qualified Data.Set as S

type Inst = (Turn, Int)
type Position = (Int, Int) -- X, Y coords
data Compass = N | S | E | W deriving Show
data Turn = L | R deriving Show

instruction :: String -> Inst
instruction (d:s) = (direction d, read s)
  where
    direction 'R' = R
    direction 'L' = L
    direction _ = error "bad direction"

f :: (Compass, Position) -> Inst -> (Compass, Position)
f (c, p) (d, s) = let d' = turn c d
                      dp = delta d' s
                      p' = (fst p + fst dp, snd p + snd dp)
                  in (d', p')
  where
    turn N L = W
    turn N R = E
    turn E L = N
    turn E R = S
    turn S L = E
    turn S R = W
    turn W L = S
    turn W R = N
    delta N s = (0, s)
    delta S s = (0, -s)
    delta E s = (s, 0)
    delta W s = (-s, 0)

explode :: [Inst] -> [Inst]
explode xs = concat $ map explode' xs
  where
    explode' (t, s) = replicate s (t, 1)

prepeat :: [Position] -> Position
prepeat ps = firstRepeat $ concat $ zipWith fillGap ps (tail ps)
  where
    range :: Int -> Int -> [Int]
    range a b | a < b     = [a..b-1]
              | otherwise = [a, a-1..b+1]
    fillGap (x, y) (x', y') | x == x'   = [ (x, y'') | y'' <- range y y' ]
                            | otherwise = [ (x'', y) | x'' <- range x x' ]
firstRepeat :: [Position] -> Position
firstRepeat ps = q' S.empty ps
  where
    q' s (p:ps) | p `S.member` s = p
                | otherwise    = q' (p `S.insert` s) ps

dist :: Position -> Int
dist (a, b) = abs a + abs b

solution :: [Inst] -> (Int, Int)
solution directions = let positions = snd <$> scanl f (N, (0,0)) directions
                          p1 = last positions
                          p2 = prepeat positions
                      in (dist p1, dist p2)
                          
main :: IO ()
main = do
  directions <- parse <$> getLine
  print $ solution directions
  
  where
    parse l = instruction <$> splitOn ", " l
