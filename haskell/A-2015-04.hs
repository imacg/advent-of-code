module Main where

import qualified Data.ByteString.Char8 as B8
import Crypto.Hash
import Crypto.Hash.Algorithms

hashes :: String -> [(Int, String)]
hashes prefix = map f [1..]
  where
    f x = (x, show $ (hash (B8.pack (prefix ++ show x)) :: Digest MD5))
    
coins :: Int -> String -> [Int]
coins n = map fst . filter (\(_, h) -> prefix == take n h) . hashes
  where
    prefix = replicate n '0'

main :: IO ()
main = do
  let input = "bgvyzdsv"
  let p1 = head (coins 5 input)
  let p2 = head (coins 6 input)
  putStrLn $ "P1 Solution: " ++ show p1
  putStrLn $ "P2 Solution: " ++ show p2
