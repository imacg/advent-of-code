{-# LANGUAGE OverloadedStrings #-}
module Main where

import qualified Data.Set as S
import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8 (endOfLine, digit, char)
import qualified Data.ByteString.Char8 as B8
import Control.Applicative

data Command = Rect Int Int
             | RotCol Int Int
             | RotRow Int Int
             
type Pixel = (Int, Int) -- x, y

rect :: Int -> Int -> S.Set Pixel
rect x y = S.fromList [ (x,y) | x <- [0..x-1], y <- [0..y-1] ]

rotRow :: Int -> Int -> Pixel -> Pixel
rotRow row b = f
  where
    f (x, y) | y == row  = ((x + b) `mod` 50, y)
             | otherwise = (x, y)


rotCol :: Int -> Int -> Pixel -> Pixel
rotCol col b = f
  where
    f (x, y) | x == col  = (x, (y + b) `mod` 6)
             | otherwise = (x, y)

parseCommands :: String -> [Command]
parseCommands s = case parseOnly parser . B8.pack $ s of Right v -> v
                                                         Left _  -> error "parse error!"
  where
    intP :: Parser Int
    intP = read <$> many1 digit
    rectP = Rect <$> (string "rect " *> intP) <*> (char 'x' *> intP)
    rotX = RotCol <$> (string "rotate column x=" *> intP) <*> (string " by " *> intP)
    rotY = RotRow <$> (string "rotate row y=" *> intP) <*> (string " by " *> intP)
    parser = many1 ((rectP <|> rotX <|> rotY) <* endOfLine)

execute :: [Command] -> S.Set Pixel
execute = execute' S.empty
  where execute' s [] = s
        execute' s ((Rect x y):cs) = execute' (s `S.union` rect x y) cs
        execute' s ((RotCol col b):cs) = execute' (S.map (rotCol col b) s) cs
        execute' s ((RotRow row b):cs) = execute' (S.map (rotRow row b) s) cs

render :: S.Set Pixel -> [String]
render s = [[ if (x,y) `S.member` s then '#' else ' ' | x <- [0..49] ] | y <- [0..5]]
main :: IO ()
main = do
  commands <- parseCommands <$> getContents
  let pixels = execute commands
  putStrLn $ "P1 Solution: " ++ show (S.size pixels)
  putStrLn "P2 Solution:"
  mapM_ putStrLn $ render pixels
