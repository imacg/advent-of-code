module Main where

import qualified Data.Set as S
import Data.List (elemIndex)

type IPv7 = ([String], [String]) -- outside, inside

chunk :: Int -> String -> [String]
chunk n s | length s < n = []
          | otherwise    = take n s : chunk n (drop 1 s)
        
abba :: String -> Bool
abba = any abba' . chunk 4
  where
    abba' s | s == replicate 4 (head s) = False
            | otherwise                 = take 2 s == (reverse . drop 2 $ s)

hasAbba :: IPv7 -> Bool
hasAbba (o, i) = any abba o && not (any abba i)

parseIp :: String -> IPv7
parseIp = outside ([], [])
  where
    outside k [] = k
    outside (o, i) xs = case '[' `elemIndex` xs of Just k  -> inside (take k xs : o, i) (drop (k+1) xs)
                                                   Nothing -> (xs : o, i)
    inside (o, i) xs = case ']' `elemIndex` xs  of Just k  -> outside (o, take k xs : i) (drop (k+1) xs)
                                                   Nothing -> error "impossible"

ssl :: IPv7 -> Bool
ssl (o, i) = let potentials = S.fromList . filter (\(x:y:z:[]) -> x == z && x /= y) . chunk 3
                 invert (x:y:z:[]) = y:x:y:[]
                 outside = S.unions . map potentials $ o
                 inside  = S.unions . map potentials $ i
             in not $ S.null $ outside `S.intersection` (S.map invert inside)
main :: IO ()
main = do
  ips <- fmap (map parseIp . lines) getContents
  putStrLn $ "P1 Solution: " ++ show (length . filter hasAbba $ ips)
  putStrLn $ "P2 Solution: " ++ show (length . filter ssl $ ips)
