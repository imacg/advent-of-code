module Main where

import Data.Char (ord, chr)
import Data.List.Split
import Data.Either (either)
import Data.List
import qualified Data.ByteString.Char8 as BS
import Data.Attoparsec.ByteString hiding (take, parse)
import Data.Attoparsec.ByteString.Char8 (char, letter_ascii, digit)
import Control.Applicative

data Room = Room { name :: String
                 , sectorId :: Int
                 , checksum :: String } deriving Show

top :: String -> String
top = map head . sortOn (\x -> - length x) . group . sort . filter (/= '-')

validSum :: Room -> Bool
validSum r = take (length (checksum r)) (top (name r)) == checksum r

roomP :: Parser Room
roomP = Room <$> roomName <*> number <*> checksum
  where
    roomName = many1 (letter_ascii <|> char '-')
    number = read <$> many1 digit
    checksum = char '[' *> many1 letter_ascii <* char ']'

parse :: String -> Room
parse s = either (error "should be impossible") id (parseOnly roomP (BS.pack s))
    
decrypt :: [Room] -> [Room]
decrypt = map changeName
  where
    remapName sid = map (\x -> if x == '-' then x else chr ((ord x - ord 'a' + sid) `mod` 26 + ord 'a'))
    changeName r@(Room name sid _) = r { name = remapName sid name }
                           
main :: IO ()
main = do
  rooms <- map parse . lines <$> getContents
  let validRooms = filter validSum rooms
  let p1answer = sum $ map sectorId validRooms
  print p1answer

  mapM_ (\r -> putStrLn $ name r ++ "," ++ show (sectorId r)) $ decrypt validRooms
