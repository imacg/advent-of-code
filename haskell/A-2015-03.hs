module Main where

import qualified Data.Set as S

type Pos = (Int, Int) -- x, y coords
type Inst = Char

houses :: [Inst] -> [Pos]
houses = scanl move (0,0)
  where
    move (x, y) '^' = (x  , y+1)
    move (x, y) 'v' = (x  , y-1)
    move (x, y) '>' = (x+1,   y)
    move (x, y) '<' = (x-1,   y)

alternate :: [a] -> [a]
alternate (x:_:xs) = x : alternate xs
alternate (x:[])   = [x]
alternate []       = []

visited :: [Inst] -> S.Set Pos
visited  = S.fromList . houses

withRobotHelper :: [Inst] -> Int
withRobotHelper inst = let santa     = alternate inst
                           robot     = alternate (tail inst)
                           allHouses = visited santa `S.union` visited robot
                       in S.size allHouses
main :: IO ()
main = do
  instructions <- getContents
  putStrLn $ "P1 Solution: " ++ show (S.size $ visited instructions)
  putStrLn $ "P2 Solution: " ++ show (withRobotHelper instructions)
